$.abitantiReg = {
  1: {
  "denominazione_regione": "Piemonte",
  "codice_regione": 1,
  "abitanti": 4356406
  },
  2: {
  "denominazione_regione": "Valle d'Aosta",
  "codice_regione": 2,
  "abitanti": 125666
  },
  3: {
  "denominazione_regione": "Lombardia",
  "codice_regione": 3,
  "abitanti": 10060574
  },
  4: {
  "denominazione_regione": "Trentino Alto Adige",
  "codice_regione": 4,
  "abitanti": 1072276
  },
  5: {
  "denominazione_regione": "Veneto",
  "codice_regione": 5,
  "abitanti": 4905854
  },
  6: {
  "denominazione_regione": "Friuli Venezia Giulia",
  "codice_regione": 6,
  "abitanti": 1215220
  },
  7: {
  "denominazione_regione": "Liguria",
  "codice_regione": 7,
  "abitanti": 1550640
  },
  8: {
  "denominazione_regione": "Emilia Romagna",
  "codice_regione": 8,
  "abitanti": 4459477
  },
  9: {
  "denominazione_regione": "Toscana",
  "codice_regione": 9,
  "abitanti": 3729641
  },
  10: {
  "denominazione_regione": "Umbria",
  "codice_regione": 10,
  "abitanti": 882015
  },
  11: {
  "denominazione_regione": "Marche",
  "codice_regione": 11,
  "abitanti": 1525271
  },
  12: {
  "denominazione_regione": "Lazio",
  "codice_regione": 12,
  "abitanti": 5879082
  },
  13: {
  "denominazione_regione": "Abruzzo",
  "codice_regione": 13,
  "abitanti": 1311580
  },
  14: {
  "denominazione_regione": "Molise",
  "codice_regione": 14,
  "abitanti": 305617
  },
  15: {
  "denominazione_regione": "Campania",
  "codice_regione": 15,
  "abitanti": 5801692
  },
  16: {
  "denominazione_regione": "Puglia",
  "codice_regione": 16,
  "abitanti": 4029053
  },
  17: {
  "denominazione_regione": "Basilicata",
  "codice_regione": 17,
  "abitanti": 562869
  },
  18: {
  "denominazione_regione": "Calabria",
  "codice_regione": 18,
  "abitanti": 1947131
  },
  19: {
  "denominazione_regione": "Sicilia",
  "codice_regione": 19,
  "abitanti": 4999891
  },
  20: {
  "denominazione_regione": "Sardegna",
  "codice_regione": 20,
  "abitanti": 1639591
  },
};

