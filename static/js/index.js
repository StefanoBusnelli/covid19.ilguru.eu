//  API Cookies
function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + '; path=/;';
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function eraseCookie(key) {
    var keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
}

//  Gestione Period

var checkPeriod = function( d0, d1 ){
  var di = new Date( d0.value );
  var df = new Date( d1.value );
  if ( di.getTime() >= df.getTime() ){
    return false;
  }

  return true;
}
var initPeriod = function(){
  var d0 = document.getElementById("data0");
  var d1 = document.getElementById("data1");
  if ( d0 != null && d1 != null ){
    var df = new Date();
    var di = new Date();
    var dx = new Date();
    di.setTime( df.getTime() - 1000*60*60*24*30 );
    d0.value = di.toISOString().substring(0, 10);
    d1.value = df.toISOString().substring(0, 10);
    d0.min = "2020-02-24";
    d1.max = d1.value;
    dx.setTime( df.getTime() - 1000*60*60*24 );
    d0.max = dx.toISOString().substring(0, 10);
    dx.setTime( di.getTime() + 1000*60*60*24 );
    d1.min = dx.toISOString().substring(0, 10);
  }
};
var updatePeriod = function(){
  var d0 = document.getElementById("data0");
  var d1 = document.getElementById("data1");
  if ( d0 != null && d1 != null ){
    if ( checkPeriod( d0, d1 ) ) {
      var df = new Date( d1.value );
      var di = new Date( d0.value );
      var dx = new Date();
      dx.setTime( df.getTime() - 1000*60*60*24 );
      d0.max = dx.toISOString().substring(0, 10);
      dx.setTime( di.getTime() + 1000*60*60*24 );
      d1.min = dx.toISOString().substring(0, 10);
      
      updateChartNat();
      updateChartReg();
    }
  }
};

//  Gestione Grafici
  
var v_plugins = { colorschemes: { scheme: 'brewer.Paired12' } };

var legendOnClick =  function( e, legendItem ){
  var index = legendItem.datasetIndex;
  var ci = this.chart;
  var c_val = [];
  ci.data.datasets.forEach(function(e, i) {
    var meta = ci.getDatasetMeta(i);
    if (meta.hidden === null)
      meta.hidden = e.hidden;
    if ( i == index )
      meta.hidden = !meta.hidden;
    c_val.push( meta.hidden );
  });
  window.pagedata.Charts[ci.canvas.id] = c_val;
  setCookie( "covid19", JSON.stringify( window.pagedata ), 30 );
  ci.update();
} 
var legendShow = function( canvas_id, data ){
  var c = window.pagedata.Charts[canvas_id];
  if( c ){
    for (var i=0; i<data.datasets.length; i++){
      data.datasets[i]['hidden'] = c[i];
    }
  }
}

var updateChartReg = function(){
  jQuery.getJSON(
    '/COVID-19/dati-json/dpc-covid19-ita-regioni.json',
    '', 
    function( data ){

      //    Filtro dati per periodo

      var d0 = document.getElementById("data0");
      var d1 = document.getElementById("data1");
      if ( d0 != null && d1 != null ){
        var di = new Date( d0.value );
        var df = new Date( d1.value );
        var data_fil = [];
        data.forEach(
          function( e ){
            var gg = new Date( e.data );
            if ( gg.getTime() >= di.getTime() && gg.getTime() <= df.getTime() )
              data_fil.push( e );
          }
        );
      } else {
        var data_fil = data;
      }
      data = data_fil;
        
      //  Aggregazione dati

      var rr= [];
      for (var i in $.abitantiReg){
        var r = $.abitantiReg[i];
        var dt= data.filter( 
          function(v){
            return v.codice_regione === r.codice_regione;
          } 
        );
        //  Province autonome Trentono AA
        if ( r.codice_regione == 4 ){
          var dr = [];
          dt.forEach(
            function(f){
              if ( dr.length == 0 ){
                f.denominazione_regione = r.denominazione_regione;
                dr.push( f );
              } else {
                if ( dr[dr.length-1].data != f.data ){
                  f.denominazione_regione = r.denominazione_regione;
                  dr.push( f );
                } else {
                  dr[dr.length-1].ricoverati_con_sintomi      += f.ricoverati_con_sintomi;
                  dr[dr.length-1].terapia_intensiva           += f.terapia_intensiva;
                  dr[dr.length-1].totale_ospedalizzati        += f.totale_ospedalizzati;
                  dr[dr.length-1].isolamento_domiciliare      += f.isolamento_domiciliare;
                  dr[dr.length-1].totale_positivi += f.totale_positivi;
                  dr[dr.length-1].variazione_totale_positivi  += f.variazione_totale_positivi;
                  dr[dr.length-1].dimessi_guariti             += f.dimessi_guariti;
                  dr[dr.length-1].deceduti                    += f.deceduti;
                  dr[dr.length-1].totale_casi                 += f.totale_casi;
                  dr[dr.length-1].tamponi                     += f.tamponi;
                }
              }
            }
          );
        } else {
          var dr = dt;
        }
        rr.push( 
          {
            denominazione_regione:  r.denominazione_regione,
            codice_regione:         r.codice_regione,
            abitanti:               r.abitanti,
            data:                   dr
          }
        );
      }

      //    --- Labels

      var d = {'datasets': [], 'labels': [] };

      d.labels = rr[0].data.map(
        function(e){
          return e.data.substring(0, 10);
        }
      );

      //    --- % Tamponi / Popolazione

      if ( $.o_charts[2].chart ){
        rr.forEach( 
          function(r){
            var dtv = { 'data': [], 'fill': false, 'label': r.denominazione_regione };
            dtv.data = r.data.map(
              function(e){
                return ( 100.0 * e.tamponi / r.abitanti ).toFixed(6);
              }
            );
            d.datasets.push( dtv );
          }
        );
        $.o_charts[2].chart.data = d;
        legendShow( "Chart002", d );
        $.o_charts[2].chart.update();
      }

      //    --- % Casi / Popolazione

      if ( $.o_charts[3].chart ){
        var d = {'datasets': [], 'labels': [] };

        d.labels = rr[0].data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );

        rr.forEach( 
          function(r){
            var dtv = { 'data': [], 'fill': false, 'label': r.denominazione_regione };
            dtv.data = r.data.map(
              function(e){
                return ( 100.0 * e.totale_positivi / r.abitanti ).toFixed(6);
              }
            );
            d.datasets.push( dtv );
          }
        );
        $.o_charts[3].chart.data = d;
        legendShow( "Chart003", d );
        $.o_charts[3].chart.update();
      }

      //    --- % Nuovi positivi / Popolazione

      if ( $.o_charts[4].chart ){
        var d = {'datasets': [], 'labels': [] };

        d.labels = rr[0].data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );

        rr.forEach( 
          function(r){
            var dtv = { 'data': [], 'fill': false, 'label': r.denominazione_regione };
            dtv.data = r.data.map(
              function(e){
                return ( 100.0 * e.variazione_totale_positivi / r.abitanti ).toFixed(6);
              }
            );
            d.datasets.push( dtv );
          }
        );
        $.o_charts[4].chart.data = d;
        legendShow( "Chart004", d );
        $.o_charts[4].chart.update();
      }

      //    --- % Deceduti / Popolazione

      if ( $.o_charts[5].chart ){
        var d = {'datasets': [], 'labels': [] };

        d.labels = rr[0].data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );

        rr.forEach( 
          function(r){
            var dtv = { 'data': [], 'fill': false, 'label': r.denominazione_regione };
            dtv.data = r.data.map(
              function(e){
                return ( 100.0 * e.deceduti / r.abitanti ).toFixed(6);
              }
            );
            d.datasets.push( dtv );
          }
        );
        $.o_charts[5].chart.data = d;
        legendShow( "Chart005", d );
        $.o_charts[5].chart.update();
      }

      //    --- % Guariti / Popolazione

      if ( $.o_charts[6].chart ){
        var d = {'datasets': [], 'labels': [] };

        d.labels = rr[0].data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );

        rr.forEach( 
          function(r){
            var dtv = { 'data': [], 'fill': false, 'label': r.denominazione_regione };
            dtv.data = r.data.map(
              function(e){
                return ( 100.0 * e.dimessi_guariti / r.abitanti ).toFixed(6);
              }
            );
            d.datasets.push( dtv );
          }
        );
        $.o_charts[6].chart.data = d;
        legendShow( "Chart006", d );
        $.o_charts[6].chart.update();
      }

      //    ---
    }
  );
};

var updateChartNat = function(){
  jQuery.getJSON(
    '/COVID-19/dati-json/dpc-covid19-ita-andamento-nazionale.json', 
    '', 
    function( data ){

      //    Filtro dati per periodo

      var d0 = document.getElementById("data0");
      var d1 = document.getElementById("data1");
      if ( d0 != null && d1 != null ){
        var di = new Date( d0.value );
        var df = new Date( d1.value );
        var data_fil = [];
        data.forEach(
          function( e ){
            var gg = new Date( e.data );
            if ( gg.getTime() >= di.getTime() && gg.getTime() <= df.getTime() )
              data_fil.push( e );
          }
        );
      } else {
        var data_fil = data;
      }
      data = data_fil;
      
      //    -----

      //    Aggiunta metriche
      data[0].tamponi_gior = null;
      for( var i=1; i<data.length; i++ ){
        data[i].tamponi_gior = data[i].tamponi - data[i-1].tamponi;
      }

      
      //    -----

      if ( $.o_charts[0].chart ){
        var d = {'datasets': [], 'labels': [] };

        d.labels = data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );
        
        var rcs = { 'data': [], 'fill': false, 'label': 'Ricoverati con sintomi' };
        rcs.data = data.map(
          function(e){
            return e.ricoverati_con_sintomi;
          }
        );
        d.datasets.push( rcs );

        var tin = { 'data': [], 'fill': false, 'label': 'Terapia Intensiva' };
        tin.data = data.map(
          function(e){
            return e.terapia_intensiva;
          }
        );
        d.datasets.push( tin );

        var tos = { 'data': [], 'fill': false, 'label': 'Totale Ospedalizzati' };
        tos.data = data.map(
          function(e){
            return e.totale_ospedalizzati;
          }
        );
        d.datasets.push( tos );

        var ido = { 'data': [], 'fill': false, 'label': 'Isolamento Domiciliare' };
        ido.data = data.map(
          function(e){
            return e.isolamento_domiciliare;
          }
        );
        d.datasets.push( ido );

        var tap = { 'data': [], 'fill': false, 'label': 'Totale positivi' };
        tap.data = data.map(
          function(e){
            return e.totale_positivi;
          }
        );
        d.datasets.push( tap );

        var nap = { 'data': [], 'fill': false, 'label': 'Variazione positivi' };
        nap.data = data.map(
          function(e){
            return e.variazione_totale_positivi;
          }
        );
        d.datasets.push( nap );

        var dgu = { 'data': [], 'fill': false, 'label': 'Dimessi guariti' };
        dgu.data = data.map(
          function(e){
            return e.dimessi_guariti;
          }
        );
        d.datasets.push( dgu );

        var dec = { 'data': [], 'fill': false, 'label': 'Deceduti' };
        dec.data = data.map(
          function(e){
            return e.deceduti;
          }
        );
        d.datasets.push( dec );

        var tca = { 'data': [], 'fill': false, 'label': 'Totale casi' };
        tca.data = data.map(
          function(e){
            return e.totale_casi;
          }
        );
        d.datasets.push( tca );

        var tam = { 'data': [], 'fill': false, 'label': 'Tamponi' };
        tam.data = data.map(
          function(e){
            return e.tamponi;
          }
        );
        d.datasets.push( tam );

        $.o_charts[0].chart.data = d;
        legendShow( "Chart000", d );
        $.o_charts[0].chart.update();
      }

      //    -----
      
      if ( $.o_charts[1].chart ){
        var d = {'datasets': [], 'labels': [] };

        d.labels = data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );

        var dtv = { 'data': [], 'fill': false, 'label': '% Totale casi / Tamponi' };
        dtv.data = data.map(
          function(e){
            return ( 100.0 * e.totale_casi / e.tamponi ).toFixed(2);
          }
        );
        d.datasets.push( dtv );

        var dtv = { 'data': [], 'fill': false, 'label': '% Totale positivi / Totale casi' };
        dtv.data = data.map(
          function(e){
            return ( 100.0 * e.totale_positivi / e.totale_casi ).toFixed(2);
          }
        );
        d.datasets.push( dtv );

        var dtv = { 'data': [], 'fill': false, 'label': '% Totale positivi / Tamponi' };
        dtv.data = data.map(
          function(e){
            return ( 100.0 * e.totale_positivi / e.tamponi ).toFixed(2);
          }
        );
        d.datasets.push( dtv );

        var dtv = { 'data': [], 'fill': false, 'label': '% Nuovi positivi / Totale casi' };
        dtv.data = data.map(
          function(e){
            return ( 100.0 * e.variazione_totale_positivi / e.totale_casi ).toFixed(2);
          }
        );
        d.datasets.push( dtv );

        var dtv = { 'data': [], 'fill': false, 'label': '% Dimessi guariti / Totale casi' };
        dtv.data = data.map(
          function(e){
            return ( 100.0 * e.dimessi_guariti / e.totale_casi ).toFixed(2);
          }
        );
        d.datasets.push( dtv );

        var dtv = { 'data': [], 'fill': false, 'label': '% Deceduti / Totale casi' };
        dtv.data = data.map(
          function(e){
            return ( 100.0 * e.deceduti / e.totale_casi ).toFixed(2);
          }
        );
        d.datasets.push( dtv );
        
        $.o_charts[1].chart.data = d;
        legendShow( "Chart001", d );
        $.o_charts[1].chart.update();
      }

    }
  );
};

//  Page setup

$(document).ready(function() { 
 
  //    Lettura cookie
  window.pagedata = JSON.parse( getCookie( "covid19" ) );
 
  //    Init cookie
  if ( !window.pagedata ) {
    window.pagedata = { 
      Charts: {
      } 
    };
  }
  if ( typeof( window.pagedata.Charts.Chart000 ) == "undefined" ){
    window.pagedata.Charts.Chart000 = [true, true, true, true, true, false, false, false, false, true];
  };
  if ( typeof( window.pagedata.Charts.Chart001 ) == "undefined" ){
    window.pagedata.Charts.Chart001 = [true, true, true, true, false, false];
  };
  setCookie( "covid19", JSON.stringify( window.pagedata ), 30 );

  //    Costanti, Funzioni grafici
  Chart.defaults.global.responsive = false;

  //    Periodo

  initPeriod();

  //    Grafici

  $.o_charts = {};

  $.o_charts[0] = {canvas: null, context: null, chart: null};
  $.o_charts[1] = {canvas: null, context: null, chart: null};
  $.o_charts[2] = {canvas: null, context: null, chart: null};
  $.o_charts[3] = {canvas: null, context: null, chart: null};
  $.o_charts[4] = {canvas: null, context: null, chart: null};
  $.o_charts[5] = {canvas: null, context: null, chart: null};
  $.o_charts[6] = {canvas: null, context: null, chart: null};

  $.o_charts[0].canvas = document.getElementById("Chart000");
  if ( $.o_charts[0].canvas != null ){
    $.o_charts[0].context = $.o_charts[0].canvas.getContext("2d");
    $.o_charts[0].chart = new Chart(
      $.o_charts[0].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Andamento Nazionale Italia', 'fontSize': 24 },
          scales: {
            yAxes: [{
              type: 'linear'
            },
            /*
            {
              type: 'logarithmic',
              ticks: {
                callback: function (value, index, values) {
                    if (value ===  100000000)  return "100M";
                    if (value ===  10000000)   return "10M";
                    if (value ===  1000000)    return "1M";
                    if (value ===  100000)     return "100K";
                    if (value ===  10000)      return "10K";
                    if (value ===  1000)       return "1K";
                    if (value ===  100)        return "100";
                    if (value ===  10)         return "10";
                    if (value ===  0)          return "0";
                    if (value === -10)         return "-10";
                    if (value === -100)        return "-100";
                    if (value === -1000)       return "-1K";
                    if (value === -10000)      return "-10K";
                    return null;
                }
              }
            }
            */
            ]
          },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[1].canvas = document.getElementById("Chart001");
  if ( $.o_charts[1].canvas != null ){
    $.o_charts[1].context = $.o_charts[1].canvas.getContext("2d");
    $.o_charts[1].chart = new Chart(
      $.o_charts[1].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Metriche Italia', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[2].canvas = document.getElementById("Chart002");
  if ( $.o_charts[2].canvas != null ){
    $.o_charts[2].context = $.o_charts[2].canvas.getContext("2d");
    $.o_charts[2].chart = new Chart(
      $.o_charts[2].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': '% Tamponi / Abitanti', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[3].canvas = document.getElementById("Chart003");
  if ( $.o_charts[3].canvas != null ){
    $.o_charts[3].context = $.o_charts[3].canvas.getContext("2d");
    $.o_charts[3].chart = new Chart(
      $.o_charts[3].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': '% Tot Positivi / Abitanti', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[4].canvas = document.getElementById("Chart004");
  if ( $.o_charts[4].canvas != null ){
    $.o_charts[4].context = $.o_charts[4].canvas.getContext("2d");
    $.o_charts[4].chart = new Chart(
      $.o_charts[4].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': '% Nuovi casi / Abitanti', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[5].canvas = document.getElementById("Chart005");
  if ( $.o_charts[5].canvas != null ){
    $.o_charts[5].context = $.o_charts[5].canvas.getContext("2d");
    $.o_charts[5].chart = new Chart(
      $.o_charts[5].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': '% Deceduti / Abitanti', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[6].canvas = document.getElementById("Chart006");
  if ( $.o_charts[6].canvas != null ){
    $.o_charts[6].context = $.o_charts[6].canvas.getContext("2d");
    $.o_charts[6].chart = new Chart(
      $.o_charts[6].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': '% Guariti / Abitanti', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  //    -----

  updateChartNat();
  updateChartReg();

});

