//  API Cookies
function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + '; path=/;';
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function eraseCookie(key) {
    var keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
}

//  Gestione Period

var checkPeriod = function( d0, d1 ){
  var di = new Date( d0.value );
  var df = new Date( d1.value );
  if ( di.getTime() >= df.getTime() ){
    return false;
  }

  return true;
}
var initPeriod = function(){
  var d0 = document.getElementById("data0");
  var d1 = document.getElementById("data1");
  if ( d0 != null && d1 != null ){
    var df = new Date();
    var di = new Date();
    var dx = new Date();
    di.setTime( df.getTime() - 1000*60*60*24*30 );
    d0.value = di.toISOString().substring(0, 10);
    d1.value = df.toISOString().substring(0, 10);
    d0.min = "2020-02-24";
    d1.max = d1.value;
    dx.setTime( df.getTime() - 1000*60*60*24 );
    d0.max = dx.toISOString().substring(0, 10);
    dx.setTime( di.getTime() + 1000*60*60*24 );
    d1.min = dx.toISOString().substring(0, 10);
  }
};
var updatePeriod = function(){
  var d0 = document.getElementById("data0");
  var d1 = document.getElementById("data1");
  if ( d0 != null && d1 != null ){
    if ( checkPeriod( d0, d1 ) ) {
      var df = new Date( d1.value );
      var di = new Date( d0.value );
      var dx = new Date();
      dx.setTime( df.getTime() - 1000*60*60*24 );
      d0.max = dx.toISOString().substring(0, 10);
      dx.setTime( di.getTime() + 1000*60*60*24 );
      d1.min = dx.toISOString().substring(0, 10);
      
      updateCharts();
    }
  }
};

//  Gestione Grafici
  
var v_plugins = { colorschemes: { scheme: 'brewer.DarkTwo6' } };

var legendOnClick = function( e, legendItem ){
  var index = legendItem.datasetIndex;
  var ci = this.chart;
  var c_val = [];
  ci.data.datasets.forEach(function(e, i) {
    var meta = ci.getDatasetMeta(i);
    if (meta.hidden === null)
      meta.hidden = e.hidden;
    if ( i == index )
      meta.hidden = !meta.hidden;
    c_val.push( meta.hidden );
  });
  window.pagedata.Charts[ci.canvas.id] = c_val;
  setCookie( "covid19", JSON.stringify( window.pagedata ), 30 );
  ci.update();
} 
var legendShow = function( canvas_id, data ){
  var c = window.pagedata.Charts[canvas_id];
  if( c ){
    for (var i=0; i<data.datasets.length; i++){
      data.datasets[i]['hidden'] = c[i];
    }
  }
}

//    -----

var f_datasets = function( labels, data ){
  var f_media = function( dt, per ){
    var data = [];
    for( var i = 0; i < dt.length; i++ ){
      var num = 0;
      var den = 0;
      for( var j = i - per + 1; j <= i; j ++ ){
        if ( j >= 0 ){
          num += dt[ j ];
          den += 1;
        }
      }
      data.push( Math.floor( num / den ) );
    }
    return data;
  };

  //  ---

  var d = {'datasets': [], 'labels': [] };
  d.labels = labels;

  //  ---

  var dts = [];
  dts[0] = { data: [], fill: false, label: 'Dati giornalieri', backgroundColor: '#1b9e7780' };
  dts[0].data = data;
  d.datasets.push( dts[0] );

  //    -----

  dts[1] = { data: [], fill: false, label: 'Media 7 giorni', type: 'line' };
  dts[1].data = f_media( dts[0].data, 7 );
  d.datasets.push( dts[1] );

  //    -----

  dts[2] = { data: [], fill: false, label: 'Media 14 giorni', type: 'line' };
  dts[2].data = f_media( dts[0].data, 14 );
  d.datasets.push( dts[2] );

  //    -----

  dts[3] = { data: [], fill: false, label: 'Media 21 giorni', type: 'line' };
  dts[3].data = f_media( dts[0].data, 21 );
  d.datasets.push( dts[3] );

  //    -----
  
  return d;    
}

var updateCharts = function(){
  jQuery.getJSON(
    '/COVID-19/dati-json/dpc-covid19-ita-regioni.json',
    '', 
    function( data ){
        
      //    Filtro dati per periodo

      var d0 = document.getElementById("data0");
      var d1 = document.getElementById("data1");
      if ( d0 != null && d1 != null ){
        var di = new Date( d0.value );
        var df = new Date( d1.value );
        var data_fil = [];
        data.forEach(
          function( e ){
            var gg = new Date( e.data );
            if ( gg.getTime() >= di.getTime() && gg.getTime() <= df.getTime() )
              data_fil.push( e );
          }
        );
      } else {
        var data_fil = data;
      }
      data = data_fil;
      
      //    -----

      var dt = data.filter( 
        function(v){
          return v.codice_regione === 3;
        } 
      );
      var lg = dt.map(
        function(e){
          return e.data.substring(0, 10);
        }
      );

      //    -----
      
      if ( $.o_charts[0].chart != null ){
        var d = f_datasets( 
          lg,
          dt.map(
            function(e){
              return e.totale_positivi;
            }
          )
        );
        $.o_charts[0].chart.data = d;
        legendShow( "ChartLomCas", d );
        $.o_charts[0].chart.update();
      }

      //    -----

      if ( $.o_charts[1].chart != null ){
        var d = f_datasets( 
          lg,
          dt.map(
            function(e){
              return e.deceduti;
            }
          )
        );
        $.o_charts[1].chart.data = d;
        legendShow( "ChartLomDec", d );
        $.o_charts[1].chart.update();
      }

      //    -----

      if ( $.o_charts[2].chart != null ){
        var d = f_datasets( 
          lg,
          dt.map(
            function(e){
              return e.dimessi_guariti;
            }
          )
        );
        $.o_charts[2].chart.data = d;
        legendShow( "ChartLomDim", d );
        $.o_charts[2].chart.update();
      }

      //    -----

      if ( $.o_charts[3].chart != null ){
        var d = f_datasets( 
          lg,
          dt.map(
            function(e){
              return e.nuovi_positivi;
            }
          )
        );
        $.o_charts[3].chart.data = d;
        legendShow( "ChartLomNuo", d );
        $.o_charts[3].chart.update();
      }

      //    -----

      if ( $.o_charts[4].chart != null ){
        var dg= dt.map(
          function(e){
            return { nuovi_positivi: e.nuovi_positivi, totale_positivi: e.totale_positivi };
          }
        );
        dd = [0.0];
        for ( var i = 1; i < dg.length; i++ ){
          dd.push( Math.round( 100.0 * ( dg[i].nuovi_positivi / dg[ i - 1 ].totale_positivi ), 2 ) );
        }
        var d = f_datasets( 
          lg,
          dd
        );
        $.o_charts[4].chart.data = d;
        legendShow( "ChartLomVNC", d );
        $.o_charts[4].chart.update();
      }

    }
  );
}

//  Page setup
$(document).ready(function() { 
 
  //    Lettura cookie
  window.pagedata = JSON.parse( getCookie( "covid19" ) );

  //    Init cookie
  if ( !window.pagedata ) {
    window.pagedata = { 
      Charts: {
      } 
    };
  }
  if ( typeof( window.pagedata.Charts.ChartLomNuo ) == "undefined" ){
    window.pagedata.Charts.ChartLomNuo = [false, false, false, false];
  };
  if ( typeof( window.pagedata.Charts.ChartLomCas ) == "undefined" ){
    window.pagedata.Charts.ChartLomCas = [false, false, true, true];
  };
  if ( typeof( window.pagedata.Charts.ChartLomDec ) == "undefined" ){
    window.pagedata.Charts.ChartLomDec = [false, false, true, true];
  };
  if ( typeof( window.pagedata.Charts.ChartLomDim ) == "undefined" ){
    window.pagedata.Charts.ChartLomDim = [false, false, true, true];
  };
  if ( typeof( window.pagedata.Charts.ChartLomVNC ) == "undefined" ){
    window.pagedata.Charts.ChartLomVNC = [false, true, true, true];
  };
  setCookie( "covid19", JSON.stringify( window.pagedata ), 30 );

  //    Costanti, Funzioni grafici
  Chart.defaults.global.responsive = false;

  //    Periodo

  initPeriod();

  //    Grafici

  $.o_charts = {};

  $.o_charts[0] = {canvas: null, context: null, chart: null};
  $.o_charts[1] = {canvas: null, context: null, chart: null};
  $.o_charts[2] = {canvas: null, context: null, chart: null};
  $.o_charts[3] = {canvas: null, context: null, chart: null};
  $.o_charts[4] = {canvas: null, context: null, chart: null};

  $.o_charts[0].canvas = document.getElementById("ChartLomCas");
  if ( $.o_charts[0].canvas != null ){
    $.o_charts[0].context = $.o_charts[0].canvas.getContext("2d");
    $.o_charts[0].chart = new Chart(
      $.o_charts[0].context,
      {
        type: 'bar',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Totale Casi Lombardia', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[1].canvas = document.getElementById("ChartLomDec");
  if ( $.o_charts[1].canvas != null ){
    $.o_charts[1].context = $.o_charts[1].canvas.getContext("2d");
    $.o_charts[1].chart = new Chart(
      $.o_charts[1].context,
      {
        type: 'bar',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Deceduti Lombardia', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[2].canvas = document.getElementById("ChartLomDim");
  if ( $.o_charts[2].canvas != null ){
    $.o_charts[2].context = $.o_charts[2].canvas.getContext("2d");
    $.o_charts[2].chart = new Chart(
      $.o_charts[2].context,
      {
        type: 'bar',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Guariti Lombardia', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }
  
  $.o_charts[3].canvas = document.getElementById("ChartLomNuo");
  if ( $.o_charts[3].canvas != null ){
    $.o_charts[3].context = $.o_charts[3].canvas.getContext("2d");
    $.o_charts[3].chart = new Chart(
      $.o_charts[3].context,
      {
        type: 'bar',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Nuovi Casi Lombardia', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }
  
  $.o_charts[4].canvas = document.getElementById("ChartLomVNC");
  if ( $.o_charts[4].canvas != null ){
    $.o_charts[4].context = $.o_charts[4].canvas.getContext("2d");
    $.o_charts[4].chart = new Chart(
      $.o_charts[4].context,
      {
        type: 'bar',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': '% Nuovi Casi / Totale Casi', 'fontSize': 24 },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  //    Popolamento dati

  updateCharts();

});

