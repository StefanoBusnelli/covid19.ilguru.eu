//  API Cookies
function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + '; path=/;';
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function eraseCookie(key) {
    var keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
}

//  Gestione Period

var checkPeriod = function( d0, d1 ){
  var di = new Date( d0.value );
  var df = new Date( d1.value );
  if ( di.getTime() >= df.getTime() ){
    return false;
  }

  return true;
}
var initPeriod = function(){
  var d0 = document.getElementById("data0");
  var d1 = document.getElementById("data1");
  if ( d0 != null && d1 != null ){
    var df = new Date();
    var di = new Date();
    var dx = new Date();
    di.setTime( df.getTime() - 1000*60*60*24*30 );
    d0.value = di.toISOString().substring(0, 10);
    d1.value = df.toISOString().substring(0, 10);
    d0.min = "2020-02-24";
    d1.max = d1.value;
    dx.setTime( df.getTime() - 1000*60*60*24 );
    d0.max = dx.toISOString().substring(0, 10);
    dx.setTime( di.getTime() + 1000*60*60*24 );
    d1.min = dx.toISOString().substring(0, 10);
  }
};
var updatePeriod = function(){
  var d0 = document.getElementById("data0");
  var d1 = document.getElementById("data1");
  if ( d0 != null && d1 != null ){
    if ( checkPeriod( d0, d1 ) ) {
      var df = new Date( d1.value );
      var di = new Date( d0.value );
      var dx = new Date();
      dx.setTime( df.getTime() - 1000*60*60*24 );
      d0.max = dx.toISOString().substring(0, 10);
      dx.setTime( di.getTime() + 1000*60*60*24 );
      d1.min = dx.toISOString().substring(0, 10);
      
      updateChartNat();
    }
  }
};

//  Gestione Grafici
  
var v_plugins = { colorschemes: { scheme: 'brewer.Paired12' } };

var legendOnClick =  function( e, legendItem ){
  var index = legendItem.datasetIndex;
  var ci = this.chart;
  var c_val = [];
  ci.data.datasets.forEach(function(e, i) {
    var meta = ci.getDatasetMeta(i);
    if (meta.hidden === null)
      meta.hidden = e.hidden;
    if ( i == index )
      meta.hidden = !meta.hidden;
    c_val.push( meta.hidden );
  });
  window.pagedata.Charts[ci.canvas.id] = c_val;
  setCookie( "covid19", JSON.stringify( window.pagedata ), 30 );
  ci.update();
} 
var legendShow = function( canvas_id, data ){
  var c = window.pagedata.Charts[canvas_id];
  if( c ){
    for (var i=0; i<data.datasets.length; i++){
      data.datasets[i]['hidden'] = c[i];
    }
  }
}

var updateChartNat = function(){
  jQuery.getJSON(
    '/COVID-19/dati-json/dpc-covid19-ita-andamento-nazionale.json', 
    '', 
    function( data ){

      //    Filtro dati per periodo

      var d0 = document.getElementById("data0");
      var d1 = document.getElementById("data1");
      if ( d0 != null && d1 != null ){
        var di = new Date( d0.value );
        var df = new Date( d1.value );
        var data_fil = [];
        data.forEach(
          function( e ){
            var gg = new Date( e.data );
            if ( gg.getTime() >= di.getTime() && gg.getTime() <= df.getTime() )
              data_fil.push( e );
          }
        );
      } else {
        var data_fil = data;
      }
      data = data_fil;
      
      //    -----

      //    Aggiunta metriche
      data[0].tamponi_gior = null;
      for( var i=1; i<data.length; i++ ){
        data[i].tamponi_gior = data[i].tamponi - data[i-1].tamponi;
      }

      //    -----

      if ( $.o_charts[0].chart ) {
        var d = {'datasets': [], 'labels': [] };

        d.labels = data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );
        
        var m_0 = { 'data': [], 'fill': false, 'label': '% Tamponi negativi' };
        m_0.data = data.map(
          function(e){
            if( e.tamponi_gior != null ){
              return (100.0 * (e.tamponi_gior - e.nuovi_positivi ) / e.tamponi_gior).toFixed(2);
            } else {
              return null;
            }
          }
        );
        d.datasets.push( m_0 );

        $.o_charts[0].chart.data = d;
        legendShow( "Chart000", d );
        $.o_charts[0].chart.update();
      }
      
      //    -----

      if ( $.o_charts[1].chart ) {
        var d = {'datasets': [], 'labels': [] };

        d.labels = data.map(
          function(e){
            return e.data.substring(0, 10);
          }
        );
        
        var m_1 = { 'data': [], 'fill': false, 'label': 'Tamponi giornalieri' };
        m_1.data = data.map(
          function(e){
            return e.tamponi_gior;
          }
        );
        d.datasets.push( m_1 );
        
        var m_2 = { 'data': [], 'fill': false, 'label': 'Tamponi positivi' };
        m_2.data = data.map(
          function(e){
            return e.nuovi_positivi;
          }
        );
        d.datasets.push( m_2 );
        
        var m_3 = { 'data': [], 'fill': false, 'label': 'Tamponi negativi' };
        m_3.data = data.map(
          function(e){
            return e.tamponi_gior - e.nuovi_positivi;
          }
        );
        d.datasets.push( m_3 );

        $.o_charts[1].chart.data = d;
        legendShow( "Chart001", d );
        $.o_charts[1].chart.update();
      }

      //    -----
      
    }
  );
};

//  Page setup

$(document).ready(function() { 
 
  //    Lettura cookie
  window.pagedata = JSON.parse( getCookie( "covid19" ) );

  //    Init cookie
  if ( !window.pagedata ) {
    window.pagedata = { 
      Charts: {
      } 
    };
  }
  if ( typeof( window.pagedata.Charts.Chart000 ) == "undefined" ){
    window.pagedata.Charts.Chart000 = [false];
  };
  if ( typeof( window.pagedata.Charts.Chart001 ) == "undefined" ){
    window.pagedata.Charts.Chart001 = [false,true,true];
  };
  setCookie( "covid19", JSON.stringify( window.pagedata ), 30 );

  //    -----

  //    Costanti, Funzioni grafici
  Chart.defaults.global.responsive = false;

  //    Periodo

  initPeriod();

  //    Grafici

  $.o_charts = {};

  $.o_charts[0] = {canvas: null, context: null, chart: null};
  $.o_charts[1] = {canvas: null, context: null, chart: null};

  $.o_charts[0].canvas = document.getElementById("Chart000");
  if ( $.o_charts[0].canvas != null ){
    $.o_charts[0].context = $.o_charts[0].canvas.getContext("2d");
    $.o_charts[0].chart = new Chart(
      $.o_charts[0].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Andamento Nazionale Italia', 'fontSize': 24 },
          scales: {
            yAxes: [{
              type: 'linear'
            },
            /*
            {
              type: 'logarithmic',
              ticks: {
                callback: function (value, index, values) {
                    if (value ===  100000000)  return "100M";
                    if (value ===  10000000)   return "10M";
                    if (value ===  1000000)    return "1M";
                    if (value ===  100000)     return "100K";
                    if (value ===  10000)      return "10K";
                    if (value ===  1000)       return "1K";
                    if (value ===  100)        return "100";
                    if (value ===  10)         return "10";
                    if (value ===  0)          return "0";
                    if (value === -10)         return "-10";
                    if (value === -100)        return "-100";
                    if (value === -1000)       return "-1K";
                    if (value === -10000)      return "-10K";
                    return null;
                }
              }
            }
            */
            ]
          },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  $.o_charts[1].canvas = document.getElementById("Chart001");
  if ( $.o_charts[1].canvas != null ){
    $.o_charts[1].context = $.o_charts[1].canvas.getContext("2d");
    $.o_charts[1].chart = new Chart(
      $.o_charts[1].context,
      {
        type: 'line',
        data: {},
        options: {
          plugins: v_plugins,
          title: { 'display': true, 'text': 'Andamento Nazionale Italia', 'fontSize': 24 },
          scales: {
            yAxes: [{
              type: 'linear'
            }]
          },
          legend: {
            onClick: legendOnClick
          }
        },
      }
    );
  }

  //    Popolamento dati

  updateChartNat();

});

